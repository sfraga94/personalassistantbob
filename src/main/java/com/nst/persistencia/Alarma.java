package com.nst.persistencia;

import java.util.Timer;
import java.util.TimerTask;

import com.nst.comunicacion.Bot;
 
public class Alarma {
    Timer timer;
    private String frase;
    private long id;
    
 
    public Alarma() {
        timer = new Timer();
        timer.schedule(new Tarea1(), 5000, 60000);
        
    }
 
    class Tarea1 extends TimerTask {
 
        @Override
		public void run() {
        	frase="";
            Solicitud solicitud = new Solicitud();
            frase = solicitud.comprobacion(1);
            if(frase!=null){
            	System.out.println("Nuevo evento: " + frase);
            	Bot.getInstance().sendMessage(Bot.getInstance().getChatId(), frase);
            }
       
    }
}
}