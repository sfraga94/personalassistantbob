package com.nst.persistencia;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateRequest;

import com.nst.modelo.Cita;
import com.nst.modelo.Conferencia;
import com.nst.modelo.Examen;
import com.nst.modelo.Persona;
import com.nst.modelo.Tarea;

public class Solicitud {
	
	public Solicitud() {
		
	}
	
	@SuppressWarnings("deprecation")
	public boolean almacenaCita(Cita cita) {
		String name = "app"+(cita.getInicio().getYear()+1900)+cita.getInicio().getMonth()+cita.getInicio().getDate()+cita.getInicio().getHours()+cita.getInicio().getMinutes()+cita.getInicio().getSeconds();
		String service = "http://localhost/inf/update";
		String query = "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#>";
		query += ("PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>");
		query += ("PREFIX time: <http://www.w3.org/2006/time#>");
		query += ("INSERT DATA { ");
		query += ("eve:"+name+" rdf:type eve:Appointment . ");
		query += ("eve:"+name+" rdf:type <http://www.w3.org/2002/07/owl#NamedIndividual> . ");
		query += ("eve:"+name+" eve:name '"+name+"' . ");
		System.out.println(name);
		if (cita.getTipo() != null) {
			query += ("eve:"+name+" eve:type '"+cita.getTipo()+"' . ");
		}
		if (cita.getInicio() != null) {
			query += (startTime(name,cita.getInicio()));
		}
		if (cita.getFin() != null) {
			query += (startTime(name,cita.getFin()));
		}
		if (cita.getPersona() != null) {
			query += (toPerson(name, cita.getPersona()));
		}
		
		query += ("eve:"+name+" eve:scope '"+cita.getAmbito()+"' . ");
		query += ("}");
		UpdateRequest ur = UpdateFactory.create(query,service);
		UpdateExecutionFactory.createRemote(ur, service).execute();
		
		return true;
	}

	@SuppressWarnings("deprecation")
	public boolean almacenaTarea(Tarea tarea) {
		String name = "task"+(tarea.getStartline().getYear()+1900)+tarea.getStartline().getMonth()+tarea.getStartline().getDate()+tarea.getStartline().getHours()+tarea.getStartline().getMinutes()+tarea.getStartline().getSeconds();
		String service = "http://localhost/inf/update";
		String query = "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#> ";
		query += ("PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> ");
		query += ("PREFIX time: <http://www.w3.org/2006/time#> ");
		query += ("INSERT DATA { ");
		query += ("eve:"+name+" rdf:type eve:Task . ");
		query += ("eve:"+name+" rdf:type <http://www.w3.org/2002/07/owl#NamedIndividual> . ");
		query += ("eve:"+name+" eve:name '"+name+"' . ");
		
		if (tarea.getAsunto() != null) {
			query += ("eve:"+name+" eve:goal '"+tarea.getAsunto()+"' . ");
		}
		if (tarea.getStartline() != null) {
			query += (startLine(name,tarea.getStartline()));
		}
		if (tarea.getDeadline() != null) {
			query += (startLine(name,tarea.getDeadline()));
		}
		query += ("}");
		System.out.println(query);
		
		UpdateRequest ur = UpdateFactory.create(query,service);
		UpdateExecutionFactory.createRemote(ur, service).execute();
		
		return true;
	}

	@SuppressWarnings("deprecation")
	public boolean almacenaExamen(Examen examen) {
		String name = "exa"+(examen.getInicio().getYear()+1900)+examen.getInicio().getMonth()+examen.getInicio().getDate()+examen.getInicio().getHours()+examen.getInicio().getMinutes()+examen.getInicio().getSeconds();
		String service = "http://localhost/inf/update";
		String query = "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#>";
		query += ("PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>");
		query += ("PREFIX time: <http://www.w3.org/2006/time#>");
		query += ("INSERT DATA { ");
		query += ("eve:"+name+" rdf:type eve:Exam . ");
		query += ("eve:"+name+" rdf:type <http://www.w3.org/2002/07/owl#NamedIndividual> . ");
		query += ("eve:"+name+" eve:name '"+name+"' . ");
		
		if (examen.getMateria() != null) {
			query += ("eve:"+name+" eve:subject '"+examen.getMateria()+"' . ");
		}
		if (examen.getTipo() != null) {
			query += ("eve:"+name+" eve:examType '"+examen.getTipo()+"' . ");
		}
		if (examen.getInicio() != null) {
			query += (startTime(name, examen.getInicio()));
		}
		query += ("eve:"+name+" eve:scope '"+examen.getAmbito()+"' . ");
		query += ("}");
		
		UpdateRequest ur = UpdateFactory.create(query,service);
		UpdateExecutionFactory.createRemote(ur, service).execute();
		
		return true;
	}

	@SuppressWarnings("deprecation")
	public boolean almacenaConferencia(Conferencia conferencia)  {
		String name = "con"+(conferencia.getInicio().getYear()+1900)+conferencia.getInicio().getMonth()+conferencia.getInicio().getDate()+conferencia.getInicio().getHours()+conferencia.getInicio().getMinutes()+conferencia.getInicio().getSeconds();
		String service = "http://localhost/inf/update";
		String query = "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#>";
		query += ("PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>");
		query += ("PREFIX time: <http://www.w3.org/2006/time#>");
		query += ("INSERT DATA { ");
		query += ("eve:"+name+" rdf:type eve:Conference . ");
		query += ("eve:"+name+" rdf:type <http://www.w3.org/2002/07/owl#NamedIndividual> . ");
		query += ("eve:"+name+" eve:name '"+name+"' . ");
		
		if (conferencia.getTema() != null) {
			query += ("eve:"+name+" eve:topic '"+conferencia.getTema()+"' . ");
		}
		if (conferencia.getInicio() != null) {
			query += (startTime(name, conferencia.getInicio()));
		}
		if (conferencia.getLugar() != null) {
			if (conferencia.getDbpediaLink() != null)
				query += (toPlace(name, conferencia.getLugar(), conferencia.getDbpediaLink()));
			else
				query += (toPlace(name, conferencia.getLugar(), "NULL"));
		}
		if (conferencia.getConferenciantes() != null) {
			Iterator<Persona> it = conferencia.getConferenciantes().iterator();
			while(it.hasNext()){
				query += (toPerson(name,it.next()));
			}
		}
		query += ("eve:"+name+" eve:scope '"+conferencia.getAmbito()+"' . ");
		query += ("}");
		
		UpdateRequest ur = UpdateFactory.create(query,service);
		UpdateExecutionFactory.createRemote(ur, service).execute();
		
		return true;
	}

	// Devolvera un String donde aparezca en cada linea el nombre del evento o tarea y su hora de inicio
	public String pregunta1(int fecha) {
		String response = "Sir, las tareas planificadas son:";
		
		GregorianCalendar date = new GregorianCalendar();
		int year = date.get(GregorianCalendar.YEAR);
		int month = (date.get(GregorianCalendar.MONTH)+1);
		int day = date.get(GregorianCalendar.DAY_OF_MONTH);
		String today = year + "-" + month + "-" + (day+fecha-1);
		String tomorrow = year + "-" + month + "-" + (day+fecha);
		
		if(month == 2 && ((day == 28 && !date.isLeapYear(year)) || (day == 29 && date.isLeapYear(year))))
			tomorrow = year + "-" + (month+1) + "-01";
		else if ((month == 4 || month == 6 || month == 9 || month == 11) && day == 30)
			tomorrow = year + "-" + (month+1) + "-01";
		else if ((month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10) && day == 31)
			tomorrow = year + "-" + (month+1) + "-01";
		else if (month == 12 && day == 31)
			tomorrow = (year+1) + "-01-01";
			
		String service = "http://localhost/inf/sparql";
		String query = "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#>"
						+ "PREFIX dat: <http://www.w3.org/2006/time#>" 
						+ "PREFIX xmls: <http://www.w3.org/2001/XMLSchema#>"
						+ "SELECT DISTINCT ?event ?st ?conf ?tema ?tipo ?asunto " 
						+ "WHERE {" 
							+ "{" 
								+ "?Event eve:name ?event . " 
								+ "?Event eve:startTime ?start . "
								+ "OPTIONAL{"
									+ "?Event eve:subject ?tema"
								+ "}"
								+ "OPTIONAL{"
									+ "?Event eve:type ?tipo"
								+ "}"
								+ "OPTIONAL{"
									+ "?Event eve:topic ?conf"
								+ "}"
							+ "}UNION{" 
								+ "?Task eve:name ?event . " 
								+ "?Task eve:startLine ?start ." 
								+ "?Task eve:goal ?asunto ."
							+ "}"
							+ "?start dat:inXSDDateTime ?st . " 
							+ "FILTER(?st >= '"+today+"T00:00:00+00:00' && ?st < '"+tomorrow+"T00:00:00+00:00')" 
						+ "}";
		QueryExecution qe = QueryExecutionFactory.sparqlService(service, query);
		ResultSet rs = qe.execSelect();

		while (rs.hasNext()) {
			QuerySolution result = rs.next();
			//System.out.println(result.get("?event") + " - " + result.get("?st").toString().split("T")[1].substring(0, 8));
			
			String anterior= "";
			if (result.get("?event").toString().contains("task")){
				if (anterior.contains(result.get("?asunto").toString()))
					response += "\n" + result.get("?asunto").toString();
				anterior = result.get("?asunto").toString();
				
			}else if(result.get("?event").toString().contains("app")){
				if (anterior.contains(result.get("?tipo").toString()))
					response += "\n Cita con " + result.get("?tipo");
				anterior = result.get("?tipo").toString();
	
			}else if(result.get("?event").toString().contains("con")){
				if (anterior.contains(result.get("?conf").toString()))
					response += "\n Conferencia sobre " + result.get("?conf");
				anterior = result.get("?conf").toString();
			
			}else{
				if (anterior.contains(result.get("?tema").toString()))
					response += "\n Examen de " + result.get("?tema").toString();
				anterior = result.get("?tema").toString();
			}
		}
		if (response.endsWith("son:"))
			return "Sir, no tiene tareas planificadas.";
		
		return response;
	}
	
	// Devuelve la proxima el tipo, el dia y la hora de la proxima consulta medica
	public String pregunta2(String tipo) {
		String response = "No tiene concertada niguna cita medica";
		
		String service = "http://localhost/inf/sparql";
		String query = "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#>"
						+ "PREFIX dat: <http://www.w3.org/2006/time#>" 
						+ "SELECT ?fecha ?tipo " 
						+ "WHERE {"
							+ "?Event eve:type ?tipo . " 
							+ "FILTER(?tipo = 'dentista' || ?tipo = 'medico' || ?tipo = 'fisio')"
							+ "?Event eve:startTime ?start ." 
							+ "?start dat:inXSDDateTime ?fecha ." 
						+ "} ORDER BY ?fecha ";
		QueryExecution qe = QueryExecutionFactory.sparqlService(service, query);
		ResultSet rs = qe.execSelect();

		while (rs.hasNext()) {
			QuerySolution result = rs.next();
			response = "Su proxima cita con el medico ("+result.get("?tipo")+") es el dia " + result.get("?fecha").toString().split("T")[0] + " a las " + result.get("?fecha").toString().split("T")[1].substring(0, 8);
		}
		return response;
	}
	
	// Devuelve una lista con las conferencias disponibles el mes que viene en Vigo
	public  String pregunta3(String ciudad) {
		
		String response = "Las conferencias disponibles son:";
		GregorianCalendar date = new GregorianCalendar();
		int year = date.get(GregorianCalendar.YEAR);
		int month = (date.get(GregorianCalendar.MONTH)+1);
		String nextMonth = year + "-" + (month+1) + "-00";
		String nextMonth2 = year + "-" + (month+2) + "-00";
		
		if (month == 12)
			nextMonth2 = (year+1) + "-01-00";
		
		
		String conference = "";
		String enlace = "";
		String service = "http://localhost/inf/sparql";
		String query = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
				+ "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#>"
				+ "PREFIX dat: <http://www.w3.org/2006/time#>" 
				+ "SELECT ?conf ?enlace ?st " 
					+ "WHERE { " +
						"?conf eve:scope ?scp ." + 
						"FILTER(?scp = 'public') ." + 
						"?conf eve:startTime ?start ." + 
						"?start dat:inXSDDateTime ?st ." + 
						"FILTER(?st >= '"+nextMonth+"T00:00:00+01:00' && ?st < '"+nextMonth2+"T00:00:00+01:00')." + 
						"?conf eve:takePlaceIn ?place." + 
						"?place eve:DBpediaLink ?enlace." +
					"}";
		QueryExecution qe = QueryExecutionFactory.sparqlService(service, query);
		ResultSet rs = qe.execSelect();

		while (rs.hasNext()) {
			QuerySolution result = rs.next();
			conference = result.get("?conf").toString();
			enlace = result.get("?enlace").toString().trim().split("/page/")[1].trim();

			service = "http://dbpedia.org/sparql";
			query = "PREFIX dbo: <http://dbpedia.org/ontology/> " 
					+ "PREFIX dbr: <http://dbpedia.org/resource/> "
					+ "SELECT ?city " 
					+ "WHERE { " 
						+ "?city dbo:city dbr:"+ciudad+" . " 
						+ "FILTER(regex(?city,dbr:" + enlace+ ")) . " 
					+ "}";
			QueryExecution qe2 = QueryExecutionFactory.sparqlService(service, query);
			ResultSet rs2 = qe2.execSelect();
	
			while (rs2.hasNext()) {
				response += "\n" + conference.split("#")[1].replace("_", " ") + " el dia " + result.get("?st").toString().split("T")[0] + " a las " + result.get("?st").toString().split("T")[1].substring(0, 8);
			}
		}
		if (response.endsWith("son:"))
			return "No hay ninguna conferencia disponible a la que pueda asistir";
		return response;
	}

	public String pregunta4(String nombreEvento) {
		
		String response = "No ha sido posible encontrar la cuenta de Twitter que ha solicitado";
		
		String service = "http://localhost/inf/sparql";
		String query = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
						+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
						+ "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#>" 
						+ "SELECT DISTINCT ?tw "
						+ "WHERE {" 
							+ "?conf eve:name ?confe ." 
							+ "FILTER( regex(?confe, 'Apple') ) ."
							+ "?conf eve:withPerson ?person ." 
							+ "?person eve:hasVCard ?p ." 
							+ "?p eve:twitterAccount ?tw" 
						+ "}";
		QueryExecution qe = QueryExecutionFactory.sparqlService(service, query);
		ResultSet rs = qe.execSelect();
		while (rs.hasNext()) {
			QuerySolution result = rs.next();
			response = "La cuenta de Twitter que ha solicitado es " + result.get("?tw");
		}
		return response;
	}

	public String pregunta5(int dia, String inicio, String fin) {
		String response = "No esta libre de " + inicio + " a " + fin + ":";
		boolean busy = false;
		
		GregorianCalendar date = new GregorianCalendar();
		date.set(GregorianCalendar.DAY_OF_MONTH, date.get(GregorianCalendar.DAY_OF_MONTH)+(dia-1));
		int year = date.get(GregorianCalendar.YEAR);
		int month = (date.get(GregorianCalendar.MONTH)+1);
		int day = date.get(GregorianCalendar.DAY_OF_MONTH);
		String start = year + "-" + month + "-" + day + "T" + inicio.split(":")[0] + ":" + inicio.split(":")[1] + ":00";
		String finish = year + "-" + month + "-" + day + "T" + fin.split(":")[0] + ":" + fin.split(":")[1] + ":00";
		
		System.out.println(start +"\n"+finish);
			
		String service = "http://localhost/inf/sparql";
		String query = "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#>"
						+ "PREFIX dat: <http://www.w3.org/2006/time#>"
						+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
						+ "PREFIX xmls: <http://www.w3.org/2001/XMLSchema#>"
						+ "SELECT DISTINCT ?event ?st ?asunto ?tema ?tipo ?conf " 
						+ "WHERE {" 
							+ "{"
								+ "?Event eve:name ?event . " 
								+ "?Event eve:startTime ?start . "
								+ "OPTIONAL{"
									+ "?Event eve:subject ?tema"
								+ "}"
								+ "OPTIONAL{"
									+ "?Event eve:type ?tipo"
								+ "}"
								+ "OPTIONAL{"
									+ "?Event eve:topic ?conf"
								+ "}"
							+ "}UNION{" 
								+ "?Task eve:name ?event . " 
								+ "?Task eve:startLine ?start ." 
								+ "?Task eve:goal ?asunto ."
							+ "}"
							+ "?start dat:inXSDDateTime ?st . " 
							+ "FILTER(?st >= '"+start+"' && ?st < '"+finish+"')" 
						+ "}";
		QueryExecution qe = QueryExecutionFactory.sparqlService(service, query);
		ResultSet rs = qe.execSelect();

		while (rs.hasNext()) {
			QuerySolution result = rs.next();
			//response += "\n" + result.get("?event") + " - " + result.get("?st").toString().split("T")[1].substring(0, 8);
			
			if (result.get("?event").toString().contains("task")){
				response += "\n" + result.get("?asunto");
				busy = true;
			}else if(result.get("?event").toString().contains("app")){
				response += "\n Cita con " + result.get("?tipo");
				busy = true;
			}else if(result.get("?event").toString().contains("con")){
				response += "\n Conferencia sobre " + result.get("?conf");
				busy = true;
			}else if(result.get("?event").toString().contains("exa")){
				response += "\n Examen de " + result.get("?tema");
				busy=true; 
			}else{
				response += "\n X" + result.get("?event").toString();
			}
		}
		if (response.endsWith(":"))
			return "Estas libre de " + inicio + " a " + fin;
		
		return response;
	}

	public String pregunta6() {
		String response = "Las citas planificadas para esta mes son:";
		GregorianCalendar date = new GregorianCalendar();
		int year = date.get(GregorianCalendar.YEAR);
		int month = (date.get(GregorianCalendar.MONTH)+1);
		String nextMonth = year + "-" + month + "-00";
		String nextMonth2 = year + "-" + (month+1) + "-00";
		
		if (month == 12)
			nextMonth2 = (year+1) + "-01-00";
		
		// Borrar
//		nextMonth = "2015-12-00";
//		nextMonth2 = "2016-01-00";
		
		String service = "http://localhost/inf/sparql";
		String query = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
				+ "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#>"
				+ "PREFIX dat: <http://www.w3.org/2006/time#>" 
				+ "SELECT ?conf ?start ?tipo " 
					+ "WHERE { "
						+ "?conf eve:type ?tipo . "
						+ "?conf eve:startTime ?st . " 
						+ "?st dat:inXSDDateTime ?start ." 
						+ "FILTER(?start >= '"+nextMonth+"' && ?start < '"+nextMonth2+"')." + 
					"}";
		QueryExecution qe = QueryExecutionFactory.sparqlService(service, query);
		ResultSet rs = qe.execSelect();

		while (rs.hasNext()) {
			QuerySolution result = rs.next();
			String appointment = result.get("?conf").toString().split("#")[1].replace("_", " ");
			String tipo = result.get("?tipo").toString();

			response += "\n" + appointment + " (" + tipo + ") el dia " + result.get("?start").toString().split("T")[0] + " a las " + result.get("?start").toString().split("T")[1].substring(0, 8);
		}
		
		if (response.endsWith("son:"))
			return "No hay ninguna cita planificada para este mes";
		return response;
	}

	public String pregunta7() {
		String response = "Has quedado a comer con:";
		
		GregorianCalendar date = new GregorianCalendar();
		int year = date.get(GregorianCalendar.YEAR);
		int month = (date.get(GregorianCalendar.MONTH)+1);
		int day = date.get(GregorianCalendar.DAY_OF_MONTH);
		String nextWeek = year + "-" + month + "-" + day;
		String nextWeek2 = year + "-" + month + "-" + (day+7);
		
		if(month == 2 && day > 21 && !date.isLeapYear(year)){
			nextWeek2 = year + "-" + (month+1) + (day + 7 - 21);
			
		}else if (month == 2 && day > 22 && date.isLeapYear(year)){
			nextWeek2 = year + "-" + (month+1) + (day + 7 - 22);
			
		}else if ((month == 4 || month == 6 || month == 9 || month == 11) && day > 24){
			nextWeek2 = year + "-" + (month+1) + (day + 7 - 24);
			
		}else if ((month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10) && day > 25){
			nextWeek2 = year + "-" + (month+1) + (day + 7 - 25);
			
		}else if (month == 12 && day > 25){
			nextWeek2 = (year+1) + "-01" + (day + 7 - 25);;
		}
		// Borrar
		nextWeek = "2015-12-00";
		nextWeek2 = "2016-01-00";
		
		String service = "http://localhost/inf/sparql";
		String query = "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>"
				+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"
				+ "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#>"
				+ "PREFIX dat: <http://www.w3.org/2006/time#>" 
				+ "SELECT ?conf ?start ?nombre " 
					+ "WHERE { "
						+ "?conf eve:name ?nombre . "
						+ "FILTER(regex(?nombre,'comer'))"
						+ "?conf eve:startTime ?st . " 
						+ "?st dat:inXSDDateTime ?start ." 
						+ "FILTER(?start >= '"+nextWeek+"' && ?start < '"+nextWeek2+"')." + 
					"}";
		QueryExecution qe = QueryExecutionFactory.sparqlService(service, query);
		ResultSet rs = qe.execSelect();

		while (rs.hasNext()) {
			QuerySolution result = rs.next();
			String appointment = result.get("?conf").toString().split("#")[1].replace("_", " ");
			String tipo = result.get("?tipo").toString();

			response += "\n" + appointment + " (" + tipo + ") el dia " + result.get("?start").toString().split("T")[0] + " a las " + result.get("?start").toString().split("T")[1].substring(0, 8);
		}
		
		if (response.endsWith("con:"))
			return "No has quedado con nadie para comer la proxima semana";
		return response;
	}

	public String pregunta8() {
		
		String enlace = "";
		String service = "http://localhost/inf/sparql";
		String query = "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#>"
						+ "PREFIX dat: <http://www.w3.org/2006/time#>" 
						+ "SELECT DISTINCT ?name ?start ?enlace " 
						+ "WHERE {"
							+ "?Event eve:name ?name . " 
							+ "?Event eve:takePlaceIn ?place . " 
							+ "?place eve:DBpediaLink ?enlace . "
							+ "?Event eve:startTime ?st . " 
							+ "?st dat:inXSDDateTime ?start . " 
						+ "} ORDER BY ?start";
		QueryExecution qe = QueryExecutionFactory.sparqlService(service, query);
		ResultSet rs = qe.execSelect();

		while (rs.hasNext()) {
			QuerySolution result = rs.next();
			if (!result.get("?enlace").toString().contains("NULL"))
				enlace = result.get("?enlace").toString().trim().split("/page/")[1].trim();

			service = "http://dbpedia.org/sparql";
			query = "PREFIX dbo: <http://dbpedia.org/ontology/> " 
					+ "PREFIX dbr: <http://dbpedia.org/resource/> "
					+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
					+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " 
					+ "SELECT DISTINCT ?country " 
					+ "WHERE {"
						+ "?place rdf:type ?c . " 
						+ "?place rdfs:label ?label . " 
						+ "?place dbo:country ?country . "
						+ "FILTER(?place = dbr:" + enlace + ") ." 
					+ "}";
			QueryExecution qe2 = QueryExecutionFactory.sparqlService(service, query);
			ResultSet rs2 = qe2.execSelect();
			while (rs2.hasNext()) {
				if (!rs2.next().toString().contains("Spain")) {
					return "El proximo evento en el extranjero sera en " + enlace;// + " (" + result.get("?name") + ")";
				}
			}
		}
		return "No hay planificado ningun viaje al extranjero";
	}

	public String pregunta9() {
		String enlace = "";
		String service = "http://localhost/inf/sparql";
		String query = "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#>"
						+ "PREFIX dat: <http://www.w3.org/2006/time#>" 
						+ "SELECT DISTINCT ?name ?start ?enlace " 
						+ "WHERE {"
							+ "?Event eve:name ?name . " 
							+ "?Event eve:takePlaceIn ?place . " 
							+ "?place eve:DBpediaLink ?enlace . "
							+ "?Event eve:startTime ?st . " 
							+ "?st dat:inXSDDateTime ?start . " 
						+ "} ORDER BY ?start";
		QueryExecution qe = QueryExecutionFactory.sparqlService(service, query);
		ResultSet rs = qe.execSelect();

		while (rs.hasNext()) {
			QuerySolution result = rs.next();
			if (!result.get("?enlace").toString().contains("NULL"))
				enlace = result.get("?enlace").toString().trim().split("/page/")[1].trim();

			service = "http://dbpedia.org/sparql";
			query = "PREFIX dbo: <http://dbpedia.org/ontology/> " 
					+ "PREFIX dbp: <http://dbpedia.org/property/> "
					+ "PREFIX dbr: <http://dbpedia.org/resource/> "
					+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
					+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " 
					+ "SELECT DISTINCT ?country ?offset "
					+ "WHERE {" 
						+ "?place rdf:type ?c . " 
						+ "?place rdfs:label ?label . "
						+ "?place dbo:country ?country . " 
						+ "FILTER(?place = dbr:" + enlace + ") ."
						+ "?country dbp:utcOffset ?offset " 
					+ "}";
			QueryExecution qe2 = QueryExecutionFactory.sparqlService(service, query);
			ResultSet rs2 = qe2.execSelect();
			while (rs2.hasNext()) {
				QuerySolution result2 = rs2.next();
				if (!result2.toString().contains("Spain")) {
					int utc = Integer.parseInt(result2.get("offset").toString().trim().split("@")[0]);
					return "El proximo evento en el extranjero sera en " + enlace + " y habra un desfase horario de " + (utc - 1) + " horas con respecto a España";				
				}
			}
		}
		return "No hay planificado ningun viaje al extranjero";
	}

	@SuppressWarnings("deprecation")
	public String pregunta10() {
		
		String response = "Los siguientes eventos estan solapados";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		
		String service = "http://localhost/inf/sparql";
		String query = "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#>"
						+ "PREFIX date: <http://www.w3.org/2006/time#>" 
						+ "SELECT DISTINCT ?event ?start ?end ?h ?m "
						+ "WHERE {" 
							+ "?Event eve:name ?event ." 
							+ "?Event eve:startTime ?st ."
							+ "?st date:inXSDDateTime ?start ." 
							+ "OPTIONAL{" 
								+ "?Event eve:endTime ?en ."
								+ "?en date:inXSDDateTime ?end" 
							+ "}OPTIONAL{" 
								+ "?Event eve:duration ?en ."
								+ "OPTIONAL{?en date:hours ?h .}" 
								+ "OPTIONAL{ ?en date:minutes ?m }" 
							+ "}" 
						+ "}";
		QueryExecution qe = QueryExecutionFactory.sparqlService(service, query);
		ResultSet rs = qe.execSelect();

		Date[] startTime = new Date[100];
		Date[] endTime = new Date[100];
		String[] events = new String[100];
		int i = 0;

		while (rs.hasNext()) {
			QuerySolution result = rs.next();
			events[i] = result.get("?event").toString();
			Date start = new Date();
			Date end = new Date();
			try {
				start = sdf.parse(result.get("start").toString());
				end = sdf.parse(result.get("start").toString());
				if (result.get("end") != null) {
					end = sdf.parse(result.get("end").toString());
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
			if (result.get("h") != null) {
				end.setHours(start.getHours() + Integer.parseInt(result.get("h").toString().substring(0, result.get("h").toString().indexOf("^^"))));
			}
			if (result.get("m") != null) {
				end.setMinutes(start.getMinutes() + Integer.parseInt(result.get("m").toString().substring(0, result.get("h").toString().indexOf("^^"))));
			}
			startTime[i] = start;
			endTime[i] = end;
			i++;
		}
		int max = i - 1;
		boolean solapados = false;

		for (i = 0; i <= max; i++) {
			for (int j = 0; j <= max; j++) {
				if (i != j) {
					if ((startTime[i].before(startTime[j]) && endTime[i].after(startTime[j]))
							|| (startTime[i].after(startTime[j]) && endTime[i].before(startTime[j]))) {
						if (!(response.contains(events[i]) && response.contains(events[j]))){
							String date1 = startTime[i].getDate() + "/" + startTime[i].getMonth() + "/" + (startTime[i].getYear()+1900) + " " + startTime[i].getHours() + ":" + startTime[i].getMinutes();
							String date2 = startTime[j].getDate() + "/" + startTime[j].getMonth() + "/" + (startTime[j].getYear()+1900) + " " + startTime[j].getHours() + ":" + startTime[j].getMinutes();
							response += "\n" + events[i] + " y " + events[j] + " (" + date1 + " - " + date2 + ")";
						}
						solapados = true;
					}
				}
			}
		}
		if (solapados)
			return response;
		else
			return "No hay ningun evento solapado";
	}

	public String pregunta11() {
		String service = "http://localhost/inf/sparql";
		String query = "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#>"
						+ "PREFIX dat: <http://www.w3.org/2006/time#>" 
						+ "SELECT ?com " 
						+ "WHERE {"
							+ "?Task eve:name ?comprar ." 
							+ "FILTER(?comprar = 'compra' || ?comprar = 'Compra') ."
							+ "?Task eve:deadLine ?dead ." 
							+ "?dead dat:inXSDDateTime ?end ."
							+ "FILTER(?end < '2015-12-14T00:00:00+00:00-0000-00-07T00:00:00+00:00') ." 
							+ "BIND (IF (EXISTS {"
								+ "?Task eve:name ?comprar ." 
								+ "FILTER(?comprar = 'compra' || ?comprar = 'Compra')"
							+ "}, 'No', 'Si') as ?com)" 
						+ "}";
		QueryExecution qe = QueryExecutionFactory.sparqlService(service, query);
		ResultSet rs = qe.execSelect();

		while (rs.hasNext()) {
			QuerySolution result = rs.next();
			return result.get("?com").toString() + " debería hacer la compra";
		}
		return "No se han encontrado compras pasadas por lo que deberia ir a hacer la compra";
	}

	public String pregunta12() {
		
		String response = "Puedes invitar a tus amigos a los siguientes eventos:";
		
		String service = "http://localhost/inf/sparql";
		String query = "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#>"
						+ "PREFIX date: <http://www.w3.org/2006/time#>" 
						+ "SELECT ?tipo ?start " 
						+ "WHERE {"
							+ "?Event eve:name ?tipo ." 
							+ "?Event eve:scope ?scp ." 
							+ "FILTER(?scp = 'private')"
							+ "?Event eve:startTime ?st ."
							+ "?st date:inXSDDateTime ?start ." 
						+ "}";
		QueryExecution qe = QueryExecutionFactory.sparqlService(service, query);
		ResultSet rs = qe.execSelect();

		while (rs.hasNext()) {
			QuerySolution result = rs.next();
			response += "\n" + result.get("?tipo").toString() + " el dia " + result.get("?start").toString().split("T")[0] + " a las " + result.get("?start").toString().split("T")[1].substring(0, 8);;
		}
		if (response.endsWith("eventos:"))
			return "No se han encontrado eventos a a los que puedas invitar a tus amigos";
		
		return response;
	}

	@SuppressWarnings("deprecation")
	public String pregunta13() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
		
		GregorianCalendar date = new GregorianCalendar();
		int year = date.get(GregorianCalendar.YEAR) - 1 ;
		int month = date.get(GregorianCalendar.MONTH) + 1;
		int day = date.get(GregorianCalendar.DAY_OF_MONTH);
		String today = year + "-" + month + "-" + day;
		
		String service = "http://localhost/inf/sparql";
		String query = "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#>"
						+ "PREFIX dat: <http://www.w3.org/2006/time#>" 
						+ "SELECT ?dentista ?start " 
						+ "WHERE {"
						+ 		"?Dentista eve:name ?dentista . "
						+		"?Dentista eve:type ?tipo . " 
						+ 		"FILTER(?tipo = 'dentista') "
						+ 		"?Dentista eve:startTime ?st . " 
						+ 		"?st dat:inXSDDateTime ?start . " 
						+	 	"FILTER(?start > '"+today+"T00:00:00+00:00') . "  
						+ "} ORDER BY DESC(?fecha) ";
		QueryExecution qe = QueryExecutionFactory.sparqlService(service, query);
		ResultSet rs = qe.execSelect();

		while (rs.hasNext()) {
			QuerySolution result = rs.next();
			String cita = "";
			try {
				Date d = sdf.parse(result.get("?start").toString());
				cita = d.getDate() + "/" + d.getMonth() + "/" + (d.getYear()+1901);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return "Debería ir al dentista el " + cita;
		}
		return "No se han encontrado citas con menos de un año por lo que le recomendamos que acuda al dentista";
	}
	
	
	public void delete(){
		String service = "http://localhost/inf/update";
		String query = "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#>"
						+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>" 
						+ "INSERT DATA { "
							+ "eve:COMIDA rdf:type eve:Task . "
							+ "eve:COMIDA rdf:type <http://www.w3.org/2002/07/owl#NamedIndividual> . "
							+ "eve:COMIDA eve:name 'COMIDA' . "
							+ "eve:COMIDA eve:goal 'HACER LA COMIDA' . "
						+ "}";
		UpdateRequest ur = UpdateFactory.create(query,service);
		UpdateExecutionFactory.createRemote(ur, service).execute();
	}
	
	
	public String comprobacion(int min) {
		String response = "Sir, las tareas planificadas que van a expirar en los proximos minutos son:";
		
		GregorianCalendar date = new GregorianCalendar();
		date.set(GregorianCalendar.MINUTE, date.get(GregorianCalendar.MINUTE)+min);
		int year = date.get(GregorianCalendar.YEAR);
		int month = (date.get(GregorianCalendar.MONTH)+1);
		int day = date.get(GregorianCalendar.DAY_OF_MONTH);
		int hour = date.get(GregorianCalendar.HOUR_OF_DAY);
		int minuto = date.get(GregorianCalendar.MINUTE);
		
		String today = year + "-" + month + "-" + day + "T" + hour + ":" + minuto;
		date.set(GregorianCalendar.MINUTE, date.get(GregorianCalendar.MINUTE)+min);
		minuto = date.get(GregorianCalendar.MINUTE);
		String tomorrow = year + "-" + month + "-" + day + "T" + hour + ":" + minuto;
			
		String service = "http://localhost/inf/sparql";
		String query = "PREFIX eve: <http://www.semanticweb.org/thor/ontologies/2015/9/events.owl#>"
						+ "PREFIX dat: <http://www.w3.org/2006/time#>" 
						+ "PREFIX xmls: <http://www.w3.org/2001/XMLSchema#>"
						+ "SELECT ?event ?st ?asunto ?tema ?tipo " 
						+ "WHERE {" 
							+ "{" 
								+ "?Event eve:name ?event . " 
								+ "?Event eve:startTime ?start . "
								+ "OPTIONAL{"
									+ "?Event eve:subject ?tema"
								+ "}"
								+ "OPTIONAL{"
									+ "?Event eve:type ?tipo"
								+ "}"
								+ "OPTIONAL{"
									+ "?Event eve:topic ?conf"
								+ "}"
							+ "}UNION{" 
								+ "?Task eve:name ?event . " 
								+ "?Task eve:startLine ?start ." 
								+ "?Task eve:goal ?asunto ."
							+ "}"
							+ "?start dat:inXSDDateTime ?st . " 
							+ "FILTER(?st >= '"+today+"' && ?st < '"+tomorrow+"')" 
						+ "}";
		QueryExecution qe = QueryExecutionFactory.sparqlService(service, query);
		ResultSet rs = qe.execSelect();

		while (rs.hasNext()) {
			QuerySolution result = rs.next();
			
			if (result.get("?event").toString().contains("task")){
				response += "\n" + result.get("?asunto");
			}else if(result.get("?event").toString().contains("app")){
				response += "\n Cita con " + result.get("?tipo");
			}else if(result.get("?event").toString().contains("con")){
				response += "\n Conferencia sobre " + result.get("?asunto");
			}else{
				response += "\n Examen de " + result.get("?tema");
			}
		}
		if (response.endsWith("son:"))
			return null;
		
		return response;
	}

	@SuppressWarnings("deprecation")
	public static String startLine(String name, Date date){
		String instance = "isl"+(date.getYear()+1900)+date.getMonth()+date.getDate()+date.getHours()+date.getMinutes();
		String fecha = (date.getYear()+1900)+"-"+(date.getMonth()+1)+"-"+date.getDate()+"T"+date.getHours()+":"+date.getMinutes()+":00+01:00";
		String result = "";
		
		result += ("time:"+instance+" rdf:type time:Instant . ");
		result += ("time:"+instance+" rdf:type <http://www.w3.org/2002/07/owl#NamedIndividual> . ");
		result += ("time:"+instance+" time:inXSDDateTime '"+fecha+"' . ");
		result += ("eve:"+name+" eve:startLine time:"+instance+" . ");
		return result;
	}
	
	@SuppressWarnings("deprecation")
	public static String startTime(String name, Date date){
		String instance = "ist"+(date.getYear()+1900)+date.getMonth()+date.getDate()+date.getHours()+date.getMinutes();
		String fecha = (date.getYear()+1900)+"-"+(date.getMonth()+1)+"-"+date.getDate()+"T"+date.getHours()+":"+date.getMinutes()+":00+01:00";
		String result = "";
		
		result += ("time:"+instance+" rdf:type time:Instant . ");
		result += ("time:"+instance+" rdf:type <http://www.w3.org/2002/07/owl#NamedIndividual> . ");
		result += ("time:"+instance+" time:inXSDDateTime '"+fecha+"' . ");
		result += ("eve:"+name+" eve:startTime time:"+instance+" . ");
		return result;
	}
	
	public static String toPlace(String name, String place, String DBpedia){
		String instance = "place"+place.replace(" ", "");
		String result = "";
		
		result += ("eve:"+instance+" rdf:type eve:Place . ");
		result += ("eve:"+instance+" rdf:type <http://www.w3.org/2002/07/owl#NamedIndividual> . ");
		result += ("eve:"+instance+" time:AddressDescription '"+place+"' . ");
		result += ("eve:"+instance+" eve:DBpediaLink '"+DBpedia+"' . ");
		result += ("eve:"+name+" eve:takePlaceIn eve:"+instance+" . ");
		return result;
	}
	
	public static String toPerson(String name, Persona person){
		String insPerson = "person"+person.getName();
		String insVCard = "vCard"+person.getName();
		String result = "";
		
		result += ("eve:"+insVCard+" rdf:type <http://www.w3.org/2001/vcard-rdf/3.0#VCard> . ");
		result += ("eve:"+insVCard+" rdf:type <http://www.w3.org/2002/07/owl#NamedIndividual> . ");
		result += ("eve:"+insVCard+" eve:twitterAccount '"+person.getCuentaTwitter()+"' . ");
		result += ("eve:"+insVCard+" eve:fn '"+person.getName()+"' . ");
		
		result += ("eve:"+insPerson+" rdf:type eve:Person . ");
		result += ("eve:"+insPerson+" rdf:type <http://www.w3.org/2002/07/owl#NamedIndividual> . ");
		result += ("eve:"+insPerson+" eve:hasVCard eve:"+insVCard+" . ");
		
		result += ("eve:"+name+" eve:withPerson eve:"+insPerson+" . ");
		return "";
	}
}
