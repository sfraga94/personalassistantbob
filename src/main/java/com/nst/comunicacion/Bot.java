package com.nst.comunicacion;

import java.util.GregorianCalendar;

import com.nst.modelo.Tarea;
import com.nst.persistencia.Alarma;
import com.nst.procesado.Response;

import co.vandenham.telegram.botapi.CommandHandler;
import co.vandenham.telegram.botapi.DefaultHandler;
import co.vandenham.telegram.botapi.MessageHandler;
import co.vandenham.telegram.botapi.TelegramBot;
import co.vandenham.telegram.botapi.requests.SendMessageRequest;
import co.vandenham.telegram.botapi.types.Message;
import co.vandenham.telegram.botapi.*;

public class Bot extends TelegramBot {

	private static final String TOKEN = "155062112:AAFH06V9eaaO7uPPHvu8_CNjp0aiT8epoNI";
	String texto;
	Boolean asunto = false, fecha = false;
	String aux1, aux2;
	static Bot instance;
	int chatId;

	public Bot(String botToken) {
		super(botToken);
		instance = this;
	}

	@CommandHandler({ "start" })
	public void handleStart(Message message) {
		texto = "Hola, soy Bob y voy a ayudar a que te organices mejor.\n";
		texto += "Para empezar te enseñare con que comandos puedes interactuar conmigo: \n";
		texto += "\n/nuevosdatos - Se muestra como se debe introducir la infomación.";
		texto += "\n/preguntar - Se muestra como formular preguntas.";
		texto += "\n/help - Se muestran los comandos.";
		texto += "\n/botinfo - Se muestra información sobre el bot y sus creadores.";
chatId=message.getChat().getId();
		replyTo(message, texto);
	}

	// This handler gets called whenever a user sends /nuevos datos
	@CommandHandler({"nuevosdatos"})
	public void handleIntroducirDatos(Message message) {
		texto = "Para comentarte como introducir los datos será mejor que divida la explicación en secciones:";
		texto += "\n\nDatos temporales:";
		texto += "\nFechas:";
		texto += "\nDD-MM-YYYY";
		texto += "\nHoras:";
		texto += "\nHH:MM";

		texto += "\n\nTarea:";
		texto += "\nRecuerdame que <Objetivo> en <Numero de minutos> minutos";
		texto += "\nRecuerdame que <Objetivo> en <Numero de horas> horas";
		texto += "\nTengo que <Objetivo> antes de <Fecha fin> a las <Deadline>";
		texto += "\nTengo que <Objetivo> antes de <Fecha fin>";

		texto += "\n\nCita:";
		texto += "\nCita <tipo> el dia <Fecha inicio> entre las <Hora inicio> y las <Hora fin>";
		texto += "\nCita <tipo> el dia <Fecha inicio> a las <Hora inicio> en <Lugar>";
		texto += "\nCita <tipo> el dia <Fecha> en <Lugar>";

		texto += "\n\nConferencia:";
		texto += "\nAsistir a la conferencia sobre <Tema> el dia <Fecha> a las <Hora> en <Lugar>";

		texto += "\n\nExamen:";
		texto += "\nTengo un examen <final / parcial> <teorico / practico> de <Materia> el dia <Fecha> a las <Hora>";
		
		texto += "\n\nModo detallado:";
		texto += "\nCrear un recordatorio";
		texto += "\nBob te preguntará lo que necesita saber";
		chatId=message.getChat().getId();
		replyTo(message, texto);
	}

	// This handler gets called whenever a user sends /preguntar
	@CommandHandler({ "preguntar" })
	public void handlePreguntar(Message message) {
		texto = "Puedes clickar en el comando o teclearlo. Tambien puedes escribir la pregunta como un mensaje."
				+ " Te mostrare algunos ejemplos del tipo de cuestiones que puedes formular:\n";
		texto += "\nCuando tengo que ir al medico?";
		texto += "\nA que conferencias puedo asistir el mes que viene en Vigo?";
		texto += "\nCual es la cuenta de Twitter del orador del proximo Apple event en la EII?";
		texto += "\nEstoy libre manana de 18:00 a 20:00?";
		texto += "\nQue citas tengo este mes?";
		texto += "\nHe quedado para comer con alguien la proxima semana?";
		texto += "\n\n Si quiere puedes plantearme algo mas complejo como:";
		texto += "\nA donde es mi proximo viaje al extranjero?";
		texto += "\nQue desfase horario tendre la proxima vez que viaje al extranjero?";
		texto += "\nTengo eventos solapados?";
		texto += "\nDeberia ir a hacer la compra?";
		texto += "\nA que eventos puedo invitar a amigos?";
		texto += "\nDeberia ir al dentista?";
		chatId=message.getChat().getId();
		replyTo(message, texto);
	}

	@CommandHandler({ "help" })
	public void handleHelp(Message message) {
		texto = "Comandos disponibles:\n";
		texto += "\n/nuevosdatos - Se muestra como se debe introducir la infomacion.";
		texto += "\n/preguntar - Se muestra como formular preguntas.";
		texto += "\n/help - Se muestran los comandos.";
		texto += "\n/botinfo - Se muestra informacion sobre el bot y sus creadores.";
		chatId=message.getChat().getId();
		replyTo(message, texto);
	}

	// This handler gets called whenever a user sends /botinfo
	@CommandHandler({ "botinfo" })
	public void handleBotInfo(Message message) {
		System.out.println(message.getText());
		texto = "Soy Bob, estoy disenado para interactuar entre el usuario y una "
				+ "base de conocimiento donde se aloja su agenda personal.";
		texto += "\nFormo parte de un proyecto para la asignatura 'Novos Servicios Telematicos'"
				+ " de la Escuela de Ingenieria de Telecomunicacion de Vigo.";
		texto += " Mis creadores son:";
		texto += "\nEloy Castelo Otero";
		texto += "\nSamuel Fraga Mateos";
		texto += "\nMicael Garcia Gonzalez";
		chatId=message.getChat().getId();
		replyTo(message, texto);
	}

	@CommandHandler({ "pregunta1", "pregunta2", "pregunta3", "pregunta4", "pregunta5", "pregunta6", "pregunta7",
			"pregunta8", "pregunta9", "pregunta10", "pregunta11", "pregunta12", "pregunta13" })
	public void handlePregunta1(Message message) {
		System.out.println(String.format("%s: %s", message.getChat().getId(), message.getText()));
		Response response = new Response(message);
		replyTo(message, response.generateResponse());
	}

	// This handler gets called whenever a user sends a general text message.
	@MessageHandler(contentTypes = Message.Type.TEXT)
	public void handleTextMessage(Message message) {
		chatId=message.getChat().getId();
		System.out.println(String.format("%s: %s", message.getChat().getId(), message.getText()));
		
		if (message.getText().equals("Crear un recordatorio")) {
			replyTo(message, "Cual es el asunto?");
			asunto = true;
		} else {
			if (asunto) {
				aux1 = message.getText();
				
				fecha = true;
				asunto = false;
				replyTo(message, "Cuando te lo recuerdo?");
			} else {

				if (fecha) {
					aux2 = message.getText();
					GregorianCalendar cal = new GregorianCalendar();
					if (aux2.equals("hoy") || aux2.equals("Hoy"))
						cal.set(GregorianCalendar.MINUTE, cal.get(GregorianCalendar.MINUTE) + 2);
					if (aux2.equals("mañana") || aux2.equals("Mañana"))
						cal.set(GregorianCalendar.DATE, cal.get(GregorianCalendar.DATE) + 1);
					Tarea tarea = new Tarea(aux1, cal);
					fecha = false;
					replyTo(message, "Te lo recordaré");
				} else {

					Response response = new Response(message);
					replyTo(message, response.generateResponse());
				}
			}
		}
	}

	// This is the default handler, called when the other two handlers don't
	// apply.
	@DefaultHandler
	public void handleDefault(Message message) {
		replyTo(message,
				"Lo siento, no entiendo ese comando. Puedes consultar como comunicarte conmigo por medio de /help.");
	}
	
	
	public static Bot getInstance(){
		return instance;
	}
	
	public int getChatId(){
		return chatId;
	}

	public static void main(String[] args) {
		TelegramBot bot = new Bot(TOKEN);
		bot.start();
		Alarma alarma = new Alarma();
		
		}

}
