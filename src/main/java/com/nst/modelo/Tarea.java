package com.nst.modelo;

import java.util.Date;
import java.util.GregorianCalendar;

public class Tarea {
	/** Variables de la clase tarea **/
	private String asunto;
	private Date startline;
	private Date deadline;
	private GregorianCalendar fechaRecordatorio;

	/** Constructores para la clase tarea **/
	public Tarea(String asunto) {
		this.asunto = asunto;
	}

	public Tarea(String asunto, Date startline) {
		this.asunto = asunto;
		this.startline = startline;
	}

	public Tarea(String asunto, GregorianCalendar fechaRecordatorio) {
		this.asunto = asunto;
		this.fechaRecordatorio = fechaRecordatorio;

	}

	public Tarea(String asunto, Date startline, Date deadline) {
		this.asunto = asunto;
		this.startline = startline;
		this.deadline = deadline;
	}

	/** Getters y setters **/
	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public Date getStartline() {
		return startline;
	}

	public void setStartline(Date startline) {
		this.startline = startline;
	}

	public Date getDeadline() {
		return deadline;
	}

	public void setDeadline(Date deadline) {
		this.deadline = deadline;
	}

}
