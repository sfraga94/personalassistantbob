package com.nst.modelo;

public class Reunion extends Evento {
	private String motivo;
	private Persona persona;
	public Reunion(String motivo, Persona persona) {
		super();
		this.motivo = motivo;
		this.persona = persona;
	}
	public Reunion(String motivo) {
		super();
		this.motivo = motivo;
	}
	public String getMotivo() {
		return motivo;
	}
	public void setMotivo(String motivo) {
		this.motivo = motivo;
	}
	public Persona getPersona() {
		return persona;
	}
	public void setPersona(Persona persona) {
		this.persona = persona;
	}

}
