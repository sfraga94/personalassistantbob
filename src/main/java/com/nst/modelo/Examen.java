package com.nst.modelo;

import java.util.Date;

public class Examen extends Evento {
	private String tipo;
	private String materia;

	public Examen(String tipo, String materia, Date fecha) {
		super(fecha);
		this.tipo = tipo;
		this.materia = materia;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getMateria() {
		return materia;
	}

	public void setMateria(String materia) {
		this.materia = materia;
	}
}
