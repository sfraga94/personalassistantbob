package com.nst.modelo;

public class Persona {
	private String name;
	private String cuentaTwitter;
	
	public Persona(String name) {
		super();
		this.name = name;
	}
	public Persona(String name, String cuentaTwitter) {
		super();
		this.name = name;
		this.cuentaTwitter = cuentaTwitter;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCuentaTwitter() {
		return cuentaTwitter;
	}
	public void setCuentaTwitter(String cuentaTwitter) {
		this.cuentaTwitter = cuentaTwitter;
	}
}
