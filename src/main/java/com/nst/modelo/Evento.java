package com.nst.modelo;

import java.util.Date;

public class Evento {
	private Date inicio;
	private Date fin;
	private int duracion;
	private String lugar;
	private String dbpediaLink;
	private String ambito;

	public Evento() {
		super();
		this.ambito = "individual";
	}

	public Evento(Date inicio) {
		super();
		this.inicio = inicio;
		this.ambito = "individual";
	}

	public Evento(Date inicio, Date fin) {
		super();
		this.inicio = inicio;
		this.fin = fin;
		this.ambito = "individual";
	}

	public Evento(Date inicio, String lugar) {
		super();
		this.inicio = inicio;
		this.lugar = lugar;
		this.ambito = "individual";
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Date getFin() {
		return fin;
	}

	public void setFin(Date fin) {
		this.fin = fin;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public String getDbpediaLink() {
		return dbpediaLink;
	}

	public void setDbpediaLink(String dbpediaLink) {
		this.dbpediaLink = dbpediaLink;
	}

	public String getAmbito() {
		return ambito;
	}

	public void setAmbito(String ambito) {
		this.ambito = ambito;
	}

}
