package com.nst.modelo;

import java.util.Date;

public class Cita extends Evento {

	private String tipo;
	private Persona persona;


	public Cita(String tipo) {
		super();
		this.tipo = tipo;
	}

	public Cita(String tipo, Persona persona) {
		super();
		this.tipo = tipo;
		this.persona = persona;
	}

	public Cita(String tipo, Persona persona, Date inicio, Date fin) {
		super(inicio, fin);
		this.persona = persona;
		this.tipo=tipo;;
	}

	public Cita(String tipo, Persona persona, Date inicio, String lugar) {
		super(inicio, lugar);
		this.persona = persona;
		this.tipo=tipo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

}
