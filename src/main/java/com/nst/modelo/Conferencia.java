package com.nst.modelo;

import java.util.ArrayList;
import java.util.Date;

public class Conferencia extends Evento {
	private String tema;
	private ArrayList<Persona> conferenciantes;

	public Conferencia(String tema, Date fechaInicio, String lugar) {
		super(fechaInicio, lugar);
		this.tema = tema;
	}

	public Conferencia(String tema, Persona conferenciante) {
		super();
		this.tema = tema;
		conferenciantes = new ArrayList<Persona>();
		conferenciantes.add(conferenciante);
	}

	public Conferencia(String tema, ArrayList<Persona> conferenciantes) {
		super();
		this.tema = tema;
		this.conferenciantes = conferenciantes;
	}

	public String getTema() {
		return tema;
	}

	public void setTema(String tema) {
		this.tema = tema;
	}

	public ArrayList<Persona> getConferenciantes() {
		return conferenciantes;
	}

	public void setConferenciantes(ArrayList<Persona> conferenciantes) {
		this.conferenciantes = conferenciantes;
	}

	public void addConferenciante(Persona conferenciante) {
		conferenciantes.add(conferenciante);
	}

}
