package com.nst.procesado;

import co.vandenham.telegram.botapi.types.Message;

public class Tokenizer {

	private Message message;
	private String[] tokens;

	public Tokenizer(Message message) {
		this.message = message;
	}

	public String[] tokenize() {
		try {
			tokens = message.getText().split(" ");
		} catch (Exception e) {
			tokens = null;
		}
		return tokens;
	}

}
