package com.nst.procesado;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.nst.modelo.Cita;
import com.nst.modelo.Conferencia;
import com.nst.modelo.Examen;
import com.nst.modelo.Persona;
import com.nst.modelo.Tarea;
import com.nst.persistencia.Solicitud;

import co.vandenham.telegram.botapi.types.Message;

public class Response {

	private Message message;
	private Date fechaInicio, fechaFin;

	public Response(Message message) {
		this.message = message;
	}

	public String generateResponse() {
		/**
		 * If para consultas
		 */
		if (compare(".*[?]") || compare("/PREGUNTA.*")) {
			/** PREGUNTAS CON VARIABLES **/
			if (compare("Que tengo que hacer .*") || compare("/PREGUNTA1")) {
				Solicitud solicitud = new Solicitud();
				String parametro = message.getText().split(" ")[4].trim();
				if (parametro.contains("hoy")) {
					return solicitud.pregunta1(1);
				} else {
					if (parametro.contains("mañana")) {
						return solicitud.pregunta1(2);
					} else {
						if (parametro.contains("pasado")) {
							return solicitud.pregunta1(3);
						}
					}
				}
				return solicitud.pregunta1(1);
			}

			if (compare("Cuando tengo que ir al medico?.*") || compare("/PREGUNTA2")) {
				Solicitud solicitud = new Solicitud();
				String parametro = message.getText().split(" ")[5].trim();
				return solicitud.pregunta2(parametro.substring(0, parametro.indexOf("?")));
			}

			if (compare("A que conferencias puedo asistir el mes que viene en Vigo?.*") || compare("/PREGUNTA3.*")) {
				Solicitud solicitud = new Solicitud();
				String parametro = message.getText().split(" ")[10].trim();
				return solicitud.pregunta3(parametro.substring(0, parametro.indexOf("?")));
			}

			if (compare("Cual es la cuenta de Twitter del orador del proximo Apple event?.*")
					|| compare("/PREGUNTA4.*")) {
				Solicitud solicitud = new Solicitud();
				String parametro = message.getText().substring(message.getText().indexOf("proximo"),
						message.getText().indexOf("?"));

				return solicitud.pregunta4(parametro);
			}

			if (compare("Estoy libre .* de .* a .*?.*") || compare("/PREGUNTA5")) {
				Solicitud solicitud = new Solicitud();
				String parametro1 = message.getText().split(" ")[2].trim();
				int dia=1;
				if (parametro1.equals("hoy")) {
					dia=1;
				} else {
					if (parametro1.equals("mañana")) {
						dia=2;
					} else {
						if (parametro1.equals("pasado")) {
							dia=3;
						}
					}
				}
				String parametro2 = message.getText().split(" ")[4].trim();
				String parametro3 = message.getText().split(" ")[6].trim();
				return solicitud.pregunta5(dia, parametro2, parametro3.substring(0, parametro3.indexOf("?")));
			}

			/** PREGUNTAS SIN VARIABLES **/
			if (compare("Que citas tengo este mes?.*") || compare("/PREGUNTA6")) {
				Solicitud solicitud = new Solicitud();
				return solicitud .pregunta6();
			}

			if (compare("He quedado para comer con alguien la proxima semana?.*") || compare("/PREGUNTA7")) {
				Solicitud solicitud = new Solicitud();
				return solicitud.pregunta7();
			}

			if (compare("A donde es mi proximo viaje al extranjero?.*") || compare("/PREGUNTA8")) {
				Solicitud solicitud = new Solicitud();
				return solicitud.pregunta8();
			}

			if (compare("Que desfase horario tendre la proxima vez que viaje al extranjero?.*")
					|| compare("/PREGUNTA9")) {
				Solicitud solicitud = new Solicitud();
				return solicitud.pregunta9();
			}

			if (compare("Tengo eventos solapados?.*") || compare("/PREGUNTA10")) {
				Solicitud solicitud = new Solicitud();
				return solicitud.pregunta10();
			}

			if (compare("Deberia ir a hacer la compra?.*") || compare("/PREGUNTA11")) {
				Solicitud solicitud = new Solicitud();
				return solicitud.pregunta11();
			}

			if (compare("A que eventos puedo invitar a amigos?.*") || compare("/PREGUNTA12")) {
				Solicitud solicitud = new Solicitud();
				return solicitud.pregunta12();
			}

			if (compare("Deberia ir al dentista?.*") || compare("/PREGUNTA13")) {
				Solicitud solicitud = new Solicitud();
				return solicitud.pregunta13();
			}

			/** INTRODUCCION DE DATOS **/
		} else {
			/** Saludo **/
			if (compare("HOLA.*")) {
				return "Hola, que puedo hacer por ti?";
			}

			/** RECORDATORIO en minutos con asunto simple **/
			if (compare("Recuerdame que .+ en .+ minutos")) {
				String asunto = message.getText().substring(message.getText().indexOf("Recuerdame que ") + 14,
						message.getText().indexOf("en") - 1);
				String minutos = message.getText().substring(message.getText().indexOf("en ") + 2,
						message.getText().indexOf("minutos") - 1);

				Calendar c = new GregorianCalendar();
				c.set(GregorianCalendar.MINUTE, c.get(GregorianCalendar.MINUTE) + Integer.parseInt(minutos.trim()));
				Tarea tarea = new Tarea(asunto, c.getTime());
				Solicitud sol = new Solicitud();
				sol.almacenaTarea(tarea);
				return "Nueva tarea con asunto: " + asunto + " para la hora " + c.getTime();
			}

			/** RECORDATORIO en horas con asunto simple **/
			if (compare("Recuerdame que .+ en .+ horas")) {
				String asunto = message.getText().substring(message.getText().indexOf("Recuerdame que ") + 14,
						message.getText().indexOf("en") - 1);
				String horas = message.getText().substring(message.getText().indexOf("en ") + 2,
						message.getText().indexOf("horas") - 1);

				Calendar c = new GregorianCalendar();
				c.set(GregorianCalendar.HOUR, c.get(GregorianCalendar.HOUR) + Integer.parseInt(horas.trim()));
				Tarea tarea = new Tarea(asunto, c.getTime());
				Solicitud sol = new Solicitud();
				sol.almacenaTarea(tarea);
				return "Nueva tarea con asunto: " + asunto + " para la hora " + c.getTime();
			}

			/** TAREA con asunto, fecha y hora **/
			if (compare("Tengo que .+ antes del .+ a las .+")) {
				String asunto = message.getText().substring(message.getText().indexOf("Tengo que ") + 10,
						message.getText().indexOf("antes") - 1);
				String dia = message.getText().substring(message.getText().indexOf("antes del ") + 9,
						message.getText().indexOf("a las") - 1);
				String hora = message.getText().substring(message.getText().indexOf("a las") + 6);

				String deadline = dia + " " + hora;
				SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				fechaFin = null;
				try {
					fechaFin = formatoDelTexto.parse(deadline);
				} catch (ParseException ex) {
					ex.printStackTrace();
				}
				if (fechaFin != null) {
					Tarea tarea = new Tarea(asunto.trim(), fechaFin);
					Solicitud sol = new Solicitud();
					sol.almacenaTarea(tarea);
					return "Nueva tarea con el asunto: " + asunto.trim() + " con fecha limite: " + fechaFin;
				} else {
					return "Lo siento, ha habido algun tipo de error.";
				}
			}

			/**
			 * TAREA simple solamente especificando el dia. SI el dia ha pasado,
			 * coge el mismo dia del siguiente mes, SI NO, coge ese dia del mes
			 * actual
			 **/

			/** TAREA con asunto y fecha **/
			if (compare("Tengo que .+ antes del .+")) {
				String asunto = message.getText().substring(message.getText().indexOf("Tengo que ") + 10,
						message.getText().indexOf("antes") - 1);
				String deadline = message.getText().substring(message.getText().indexOf("antes del ") + 9);
				SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
				fechaFin = null;
				try {
					fechaFin = formatoDelTexto.parse(deadline);
				} catch (ParseException ex) {
					ex.printStackTrace();
				}
				if (fechaFin != null) {
					Tarea tarea = new Tarea(asunto.trim(), fechaFin);
					Solicitud sol = new Solicitud();
					sol.almacenaTarea(tarea);
					return "Nueva tarea con el asunto: " + asunto.trim() + " con fecha limite: " + fechaFin;
				} else {
					return "Lo siento, ha habido algun tipo de error.";
				}

			}

			/**
			 * CITA con persona, entre una fecha de inicio y una fecha de
			 * finalizacion
			 **/
			if (compare("Cita .+ el dia .+ entre las .+ y las .+")) {
				String tipo = message.getText().split(" ")[1].trim();
				String dia = message.getText().split(" ")[4].trim();
				String horaInicio = message.getText().split(" ")[7].trim();
				String horaFin = message.getText().split(" ")[10].trim();

				Persona persona = null;//new Persona(tipo.trim());

				String deadline = dia + " " + horaFin;
				SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				fechaFin = null;
				try {
					fechaFin = formatoDelTexto.parse(deadline);
				} catch (ParseException ex) {
					ex.printStackTrace();
				}

				String startline = dia + " " + horaInicio;
				formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				fechaInicio = null;
				try {
					fechaInicio = formatoDelTexto.parse(startline);
				} catch (ParseException ex) {
					ex.printStackTrace();
				}

				if (fechaFin != null && fechaInicio != null) {
					Cita cita = new Cita(tipo, persona, fechaInicio, fechaFin);
					Solicitud sol = new Solicitud();
					sol.almacenaCita(cita);
					return "Nueva cita desde " + cita.getInicio() + " hasta "+ cita.getFin();
				} else {
					return "Lo siento, ha habido algun tipo de error.";
				}
			}

			/** CITA con persona en una fecha en un sitio **/
			if (compare("Cita con .+ el dia .+ a las .+ en .+")) {
				String nombre = message.getText().substring(message.getText().indexOf("Cita con ") + 8,
						message.getText().indexOf("el dia") - 1);
				String dia = message.getText().substring(message.getText().indexOf("el dia ") + 7,
						message.getText().indexOf("a las") - 1);
				String horaInicio = message.getText().substring(message.getText().indexOf("a las ") + 5,
						message.getText().indexOf(" en ") - 1);
				String lugar = message.getText().substring(message.getText().indexOf(" en ") + 3);

				Persona persona = new Persona(nombre.trim());

				String startline = dia + " " + horaInicio;
				SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				fechaInicio = null;
				try {
					fechaInicio = formatoDelTexto.parse(startline);
				} catch (ParseException ex) {
					ex.printStackTrace();
				}

				if (fechaInicio != null) {
					Cita cita = new Cita(nombre, persona, fechaInicio, lugar.trim());
					Solicitud sol = new Solicitud();
					sol.almacenaCita(cita);
					return "Nueva cita: " + cita.getPersona().getName() + " el dia " + cita.getInicio() + " en "
							+ cita.getLugar();
				} else {
					return "Lo siento, ha habido algun tipo de error.";
				}
			}

			/** CONFERENCIA sobre un tema un dia en un sitio determinado **/
			if (compare("Asistir a la conferencia sobre .+ el dia .+ a las .+ en .+")) {
				String tema = message.getText().substring(message.getText().indexOf("conferencia sobre ") + 17,
						message.getText().indexOf("el dia ") - 1);
				String dia = message.getText().substring(message.getText().indexOf("el dia ") + 7,
						message.getText().indexOf("a las") - 1);
				String horaInicio = message.getText().substring(message.getText().indexOf(" a las ") + 6,
						message.getText().indexOf(" en ") - 1 );
				String lugar = message.getText().substring(message.getText().indexOf(" en ") + 3);

				String startline = dia + " " + horaInicio;
				SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				fechaInicio = null;
				try {
					fechaInicio = formatoDelTexto.parse(startline);
				} catch (ParseException ex) {
					ex.printStackTrace();
				}

				if (fechaInicio != null) {
					Conferencia conferencia = new Conferencia(tema, fechaInicio, lugar.trim());
					Solicitud sol = new Solicitud();
					sol.almacenaConferencia(conferencia);
					return "Nueva conferencia sobre: " + conferencia.getTema() + " el dia " + conferencia.getInicio()
							+ " en " + conferencia.getLugar();
				} else {
					return "Lo siento, ha habido algun tipo de error.";
				}
			}

			/** EXAMEN con tipo de una materia a una dia y una hora **/
			if (compare("Tengo un examen .+ de .+ el dia .+ a las .+")) {
				String tipo = message.getText().substring(message.getText().indexOf("un examen ") + 9,
						message.getText().indexOf("de") - 1);
				String materia = message.getText().substring(message.getText().indexOf("de") + 2,
						message.getText().indexOf("el dia") - 1);
				String dia = message.getText().substring(message.getText().indexOf("el dia ") + 7,
						message.getText().indexOf("a las") - 1);
				String horaInicio = message.getText().substring(message.getText().indexOf("a las ") + 5);

				String startline = dia + " " + horaInicio;
				SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy HH:mm");
				fechaInicio = null;
				try {
					fechaInicio = formatoDelTexto.parse(startline);
				} catch (ParseException ex) {
					ex.printStackTrace();
				}

				if (fechaInicio != null) {
					Examen examen = new Examen(tipo.trim(), materia.trim(), fechaInicio);
					Solicitud sol = new Solicitud();
					sol.almacenaExamen(examen);
					return "Nuevo examen " + examen.getTipo() + " de " + examen.getMateria() + " en fecha "
							+ examen.getInicio();
				} else {
					return "Lo siento, ha habido algun tipo de error.";
				}
			}
		}
		return "Lo siento, no te he entenido. Puedes consultar como comunicarte conmigo por medio de /help.";
	}

	/**
	 * El metodo compara la expresion regular que se le pasa con el texto del
	 * mensaje
	 * 
	 * @param patternString
	 *            expresion regular
	 * @return true si son iguales, false en caso contrario
	 *         http://programacion.net/articulo/
	 *         expresiones_regulares_en_java_127
	 */
	private boolean compare(String patternString) {
		String messageTextUpperCase = message.getText().toUpperCase().trim();
		// Atento a el toUpperCase() aqui abajo para evitar tener que escribir
		// arriba las preguntas en mayus
		Pattern pattern = Pattern.compile(patternString.toUpperCase().trim());
		Matcher matcher = pattern.matcher(messageTextUpperCase);
		return matcher.matches();
	}
}
